<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/home');
});

Route::get('/keluar', function(){
    \Auth::logout();

    return redirect('/login');
});

//auth
Route::get('/auth', 'AuthController@login');
Route::post('/postLogin', 'AuthController@postLogin');

//ADMIN dashboard
    Route::get('/dashboard', 'DashboardController@index');
//master barang
    Route::get('/barang','BarangController@index')->name('tampil_barang');
    Route::get('/barang/create','BarangController@createData')->name('create_barang');
    Route::post('/barang/insert','BarangController@insertData')->name('insert_barang');
    Route::get('/barang/edit/{id}','BarangController@editData')->name('edit_barang');
    Route::post('/barang/edit_aksi', 'BarangController@editAksi');
    Route::get('/barang/delete/{id}', 'BarangController@deleteData');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
