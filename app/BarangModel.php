<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarangModel extends Model
{
    protected $table = 'barang';

    protected $fillable = [
        'nama_barang','harga_satuan','stok','is_active','keterangan',
    ];
}
