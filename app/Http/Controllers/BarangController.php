<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BarangModel;

class BarangController extends Controller
{
    public function index()
    {
        $data = BarangModel::all()
                    ->where('is_active','1');
        //return view('master_barang.index', compact('data'));
        return view('admin.barang.index', compact('data'));
    }

    public function createData()
    {
        return view('admin.barang.formTambahBarang');
    }

    public function insertData(Request $request, BarangModel $brgModel )
    {
        $simpan = $brgModel->create([
            'nama_barang'   => $request->nama_barang,
            'harga_satuan'  => $request->harga_satuan,
            'stok'          => $request->stok_barang,
            'keterangan'    => $request->keterangan,
            'is_active'     => 1,
            
        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_barang')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_barang')->with('success','data berhasil disimpan');
    }

    public function editData($id)
    {   
        $barang = BarangModel::where('kode_barang',$id)->first();
        return view('admin.barang.formEditBarang', compact('barang'));
    }

    public function editAksi(Request $request)
    {
        //update aksi, simpan data update
        $data = array(  
                'kode_barang'   => $request->kode_barang,
                'nama_barang'   => $request->nama_barang,
                'harga_satuan'  => $request->harga_satuan,
                'stok'          => $request->stok_barang,
                'keterangan'    => $request->keterangan
                
             );

        BarangModel::where('kode_barang', $request->kode_barang)->update($data);
        return redirect('/barang');
            
    }

    public function deleteData($id)
    {
        BarangModel::where('kode_barang', $id)->delete();
        return redirect('/barang');
    }

    public function softDelete($id, BarangModel $barangModel)
    {
        $simpan = $barangModel->where('kode_barang', $id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_barang')->with('error','data gagal dihapus');
        }

        return redirect()->route('tampil_barang')->with('success','data berhasil dihapus');
    }

}
