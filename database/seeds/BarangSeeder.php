<?php

use Illuminate\Database\Seeder;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barang')->insert(array(
            array(
                'nama_barang'   => 'Handuk',
                'harga_satuan'  => 30000,
                'stok'          => 10,
                'keterangan'    => 'Produk Terbaik',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
            array(
                'nama_barang'   => 'Sabun Mandi',
                'harga_satuan'  => 4000,
                'stok'          => 15,
                'keterangan'    => 'Produk Terlaris',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
            array(
                'nama_barang'   => 'Smartphone',
                'harga_satuan'  => 1224000,
                'stok'          => 30,
                'keterangan'    => 'Produk Terlaris',
                'is_active'     => 1,
                'created_at'    => now(),
            ),
        ));
    }
}
