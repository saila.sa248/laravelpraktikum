<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(array(
            array(
                'name'   => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('admin'),
                'created_at' => now(),
            )
        ));
    }
}