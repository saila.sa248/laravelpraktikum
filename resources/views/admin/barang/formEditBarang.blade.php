@extends('admin.layouts.master')
@section('content')
    <div class="main-content">
    <div class="section">
    <div class="row">
        <div class="col-md-12 ">
            <div class="card">
            <div class="card-header">
                <h3>Form Edit Barang</h3>
            </div>
            </div>
            <div class="card mt-3">
                <div class="card-body">
                    <form action="/barang/edit_aksi" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="kode_barang" value="{{ $barang->kode_barang }}">
                            <label for="">Nama Barang</label>  
                            <input class="form-control" type="text" name="nama_barang" id="nama_barang" value="{{ $barang->nama_barang }}"> 
                        </div>
                        <div class="form-group">
                            <label for="">Harga Satuan</label> 
                            <input class="form-control" type="number" name="harga_satuan" id="harga_satuan" value="{{ $barang->harga_satuan }}">
                        </div>
                        <div class="form-group">
                            <label for="">Stok</label> 
                            <input class="form-control" type="number" name="stok_barang" id="stok_barang" value="{{ $barang->stok }}"> 
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan</label>
                            <input class="form-control" type="text" name="keterangan" id="keterangan" value="{{ $barang->keterangan }}">
                        </div>

                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    
                                    <a href="{{ route('tampil_barang') }}"><input class="btn btn-dark form-control" type="button" value="Kembali"></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="btn btn-primary form-control mb-2" type="submit" value="Simpan">
                                </div>
                            </div>
                        </div>
                    </form>
    
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection

