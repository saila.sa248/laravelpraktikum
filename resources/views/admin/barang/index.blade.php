@extends('admin.layouts.master')

@section('content')
    <div class="main-content">
        <div class="section">
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <a class="btn btn-primary" href="{{ route('create_barang') }}">Tambah Barang</a>
                        </div>

                        <div class="card-body">
                            <table class="table table-hover table-responsive">
                                <thead>
                                    <tr>
                                    <th scope="col">NO</th>
                                    <th scope="col">NAMA BARANG</th>
                                    <th scope="col">HARGA SATUAN</th>
                                    <th scope="col">STOK</th>
                                    <th scope="col">KETERANGAN</th>
                                    <!-- <th scope="col">Aktif</th> -->
                                    <th scope="col">AKSI</th>
                                    </tr>
                                </thead>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach( $data as $row )
                                <tbody>
                                    <tr>
                                    <th scope="row">{{ $i++ }}</th>
                                    <td>{{ $row->nama_barang }}</td>
                                    <td>{{ $row->harga_satuan }}</td>
                                    <td>{{ $row->stok }}</td>
                                    <td>{{ $row->keterangan }}</td>
                                    <!-- <td>{{ $row->is_active }}</td> -->
                                    <td>
                                        <div class="row">
                                        <div class="col-md-6">
                                            <a class="btn btn-sm btn-primary" href="{{ route('edit_barang', $row->kode_barang) }}"><i class="fas fa-edit"></i></a> 
                                        </div>
                                        <div class="col-md-6">
                                            <a class="btn btn-sm btn-danger" href="barang/delete/{{ $row->kode_barang }}" onclick="return confirm('Yakin Mau Dihapus?')"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                        </div>
                                    </td>
                                    </tr>
                                </tbody>
                            @endforeach
                            </table>

                            <div class="card-body">
                                <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="col-lg-3 col-md-3 ">
                    <div class="card card-statistic-2">
                        <div class="card-header">
                            <h6 align="center">Jumlah Barang</h6>
                        </div>
                        
                            <div class="card-icon shadow-primary bg-primary ml-5">
                                <i class="fas fa-shopping-bag"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-body">
                                    <h1>{{count($data)}}</h1>
                                </div>
                            </div>
                        
                    </div>
                </div>
                   
            </div>
        </div>
    </div>
@endsection