@extends('admin.layouts.master')

    @section('content')
    <div class="main-content">
        <section class="section">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-stats">
                  <div class="card-stats-title">Order Statistics -
                    <div class="dropdown d-inline">
                      <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">August</a>
                      <ul class="dropdown-menu dropdown-menu-sm">
                        <li class="dropdown-title">Select Month</li>
                        <li><a href="#" class="dropdown-item">January</a></li>
                        <li><a href="#" class="dropdown-item">February</a></li>
                        <li><a href="#" class="dropdown-item">March</a></li>
                        <li><a href="#" class="dropdown-item">April</a></li>
                        <li><a href="#" class="dropdown-item">May</a></li>
                        <li><a href="#" class="dropdown-item">June</a></li>
                        <li><a href="#" class="dropdown-item">July</a></li>
                        <li><a href="#" class="dropdown-item active">August</a></li>
                        <li><a href="#" class="dropdown-item">September</a></li>
                        <li><a href="#" class="dropdown-item">October</a></li>
                        <li><a href="#" class="dropdown-item">November</a></li>
                        <li><a href="#" class="dropdown-item">December</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-stats-items">
                    <div class="card-stats-item">
                      <div class="card-stats-item-count">24</div>
                      <div class="card-stats-item-label">Pending</div>
                    </div>
                    <div class="card-stats-item">
                      <div class="card-stats-item-count">12</div>
                      <div class="card-stats-item-label">Shipping</div>
                    </div>
                    <div class="card-stats-item">
                      <div class="card-stats-item-count">23</div>
                      <div class="card-stats-item-label">Completed</div>
                    </div>
                  </div>
                </div>
                <div class="card-icon shadow-primary bg-primary">
                  <i class="fas fa-archive"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Jumlah Barang</h4>
                  </div>
                  <div class="card-body">
                    
                      {{ count($data) }}
                  
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-chart">
                  <canvas id="balance-chart" height="80"></canvas>
                </div>
                <div class="card-icon shadow-primary bg-primary">
                  <i class="fas fa-dollar-sign"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Balance</h4>
                  </div>
                  <div class="card-body">
                    $187,13
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
              <div class="card card-statistic-2">
                <div class="card-chart">
                  <canvas id="sales-chart" height="80"></canvas>
                </div>
                <div class="card-icon shadow-primary bg-primary">
                  <i class="fas fa-shopping-bag"></i>
                </div>
                <div class="card-wrap">
                  <div class="card-header">
                    <h4>Sales</h4>
                  </div>
                  <div class="card-body">
                    4,732
                  </div>
                </div>
              </div>
            </div>
          </div>

          
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">

                <!-- TABLE -->
                
                  <div class="card-header">
                      <a class="btn btn-primary" href="{{ route('create_barang') }}">Tambah Barang</a>
                  </div>
                  <div class="card-body">
                  
                    <table class="table table-hover table-responsive">
                      <thead>
                        <tr>
                          <th scope="col">NO</th>
                          <th scope="col">NAMA BARANG</th>
                          <th scope="col">HARGA SATUAN</th>
                          <th scope="col">STOK</th>
                          <th scope="col">KETERANGAN</th>
                          <th scope="col">Aktif</th>
                          <th scope="col">AKSI</th>
                        </tr>
                      </thead>
                      @php
                        $i = 1;
                      @endphp
                      @foreach( $data as $row )
                      <tbody>
                        <tr>
                          <th scope="row">{{ $i++ }}</th>
                          <td>{{ $row->nama_barang }}</td>
                          <td>{{ $row->harga_satuan }}</td>
                          <td>{{ $row->stok }}</td>
                          <td>{{ $row->keterangan }}</td>
                          <td>{{ $row->is_active }}</td>
                          <td>
                            <div class="row">
                              <div class="col-md-6">
                                <a class="btn btn-sm btn-primary" href="{{ route('edit_barang', $row->kode_barang) }}"><i class="fas fa-edit"></i></a> 
                              </div>
                              <div class="col-md-6">
                                <a class="btn btn-sm btn-danger" href="barang/delete/{{ $row->kode_barang }}" onclick="return confirm('Yakin Mau Dihapus?')"><i class="fas fa-trash-alt"></i></a>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                      @endforeach
                    </table>
                  </div>
                

                
              </div>
            </div>
           
          </div>
        
          
        </section>
    </div>
    @endsection