<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h2>Edit Data Barang</h2>
    
    <form action="/barang/edit_aksi" method="post">
        @csrf
        <input type="hidden" name="kode_barang" value="{{ $barang->kode_barang }}">
        Nama Barang 
        <input type="text" name="nama_barang" id="nama_barang" value="{{ $barang->nama_barang }}"> <br><br>
        Harga Satuan
        <input type="number" name="harga_satuan" id="harga_satuan" value="{{ $barang->harga_satuan }}"><br><br>
        Stok
        <input type="number" name="stok_barang" id="stok_barang" value="{{ $barang->stok }}"> <br><br>
        Keterangan
        <input type="text" name="keterangan" id="keterangan" value="{{ $barang->keterangan }}"><br><br>

        <input type="button" value="Kembali">
        <input type="submit" value="Simpan">
    </form>
    
</body>
</html>